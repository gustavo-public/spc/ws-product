package br.org.spcbrasil.webflux.wsproduct.repository;

import br.org.spcbrasil.webflux.wsproduct.domain.Product;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ProductRepository extends ReactiveMongoRepository<Product,String> {
}
