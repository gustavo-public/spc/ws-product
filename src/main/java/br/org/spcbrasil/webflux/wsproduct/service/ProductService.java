package br.org.spcbrasil.webflux.wsproduct.service;

import br.org.spcbrasil.webflux.wsproduct.domain.Product;
import reactor.core.publisher.Mono;

public interface ProductService {

    Mono<Product>create(Product product);

    Mono<Product>findById(String id);

}
