package br.org.spcbrasil.webflux.wsproduct.service.impl;

import br.org.spcbrasil.webflux.wsproduct.domain.Product;
import br.org.spcbrasil.webflux.wsproduct.repository.ProductRepository;
import br.org.spcbrasil.webflux.wsproduct.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;

    @Override
    public Mono<Product> create(Product product) {
        return Mono.just(product)
                    .doOnNext(p -> p.setId(UUID.randomUUID().toString()))
                    .flatMap(repository::save);
    }

    @Override
    public Mono<Product> findById(String id) {
        return repository.findById(id);
    }
}
