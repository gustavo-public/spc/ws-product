package br.org.spcbrasil.webflux.wsproduct.domain;

import lombok.*;
import org.springframework.data.annotation.Id;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {

    @Id
    private String id;
    private String name;
    private String category;

}
