package br.org.spcbrasil.webflux.wsproduct.controller;

import br.org.spcbrasil.webflux.wsproduct.domain.Product;
import br.org.spcbrasil.webflux.wsproduct.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "ws-product/api/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService service;


    @GetMapping
    public Mono<Product>findById(@RequestParam String  id){
        return service.findById(id);
    }

    @PostMapping
    public Mono<Product>create(@RequestBody Product product){
        return service.create(product);
    }

}
